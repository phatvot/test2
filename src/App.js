import { useState } from "react";
import Papa from "papaparse";
import "./App.css";

function App() {
  const [data, setData] = useState([]);

  // takes an array of button sequences and returns time per robot run.
  const runTime = (buttons) => {
    // Initialize the variables for robotA and robotB positions and time for both robot
    let robotA = 1;
    let robotB = 1;
    let timeA = 0;
    let timeB = 0;

    // Loop through the array of button sequences
    for (let i = 0; i < buttons.length; i++) {
      // Get the current button sequence and split it into robot name and button number
      let sequence = buttons[i];
      let robot = sequence[0];
      let button = parseInt(sequence.slice(1));

      // Check which robot
      if (robot === "A") {
        // Calculate the distance between robotA's current position and the button
        let distance = Math.abs(robotA - button);

        // Add the distance and one second for pushing the button to the time
        timeA += distance + 1;

        // Update robotA's position to the button
        robotA = button;
      } else if (robot === "B") {
        // Calculate the distance between robotB's current position and the button
        let distance = Math.abs(robotB - button);

        // Add the distance and one second for pushing the button to the time
        timeB += distance + 1;

        // Update robotB's position to the button
        robotB = button;
      }
    }

    // Return the minimum time required by the robots can run
    return Math.max(timeA, timeB) + 1;
  };

  // handle file upload
  const handleFileChange = (e) => {
    Papa.parse(e.target.files[0], {
      quoteChar: '"',
      escapeChar: '"',
      header: false,
      transformHeader: undefined,
      dynamicTyping: false,
      preview: 0,
      encoding: "",
      step: (row) => {
        setData((prevData) => [...prevData, row.data]); // add the data object to the state array
      },
    });
  };

  // generate the data to view
  const generateData = (arr) => {
    /* display the data for each row */
    return arr.map((item, index) => (
      <div className="align-right" key={index}>
        {JSON.stringify(item).replace(/]|[[]|"/g, "")}
      </div>
    ));
  };

  // generate the result to view
  const calculateRobotTimeRun = (arr) => {
    return arr.map((item, index) => (
      <div key={index}>
        {/* display the data for each row */}
        Case #{index + 1}: {runTime(item)}
      </div>
    ));
  };

  return (
    <div className="App">
      <h1>
        Calculate the minimum time required by the robots to complete the run
      </h1>
      <small>Please import the file data.csv from source code</small>
      <input type="file" accept=".csv" onChange={handleFileChange} />
      <h5>Input</h5>
      {generateData(data)}
      <h5>Output</h5>
      {calculateRobotTimeRun(data)}
    </div>
  );
}

export default App;
